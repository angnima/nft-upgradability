//SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "./PepeNFT.sol";

contract PepeNFTv2 is PepeNFT {

    function test() pure public returns(string memory) {
        return "upgraded";
    }
}