const { expect } = require("chai");
const { ethers } = require("hardhat");
const hre = require("hardhat");

describe("ERC721 Upgradeable", function () {
  it("Should deploy an upgradeable ERC721 Contract", async function () {
    const PepeNFT = await ethers.getContractFactory("PepeNFT");
    const PepeNFTv2 = await ethers.getContractFactory("PepeNFTv2");

    let proxyContract = await hre.upgrades.deployProxy(PepeNFT, {
      kind: "uups",
    });
    const [owner] = await ethers.getSigners();
    const ownerOfToken1 = await proxyContract.ownerOf(1);

    expect(ownerOfToken1).to.equal(owner.address);

    proxyContract = await hre.upgrades.upgradeProxy(proxyContract, PepeNFTv2);
    expect(await proxyContract.test()).to.equal("upgraded");
  });
});
